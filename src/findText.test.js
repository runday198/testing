const getFileContent = require("./getFileContent");
const findText = require("./findText");
jest.mock("./getFileContent");

describe("findText", function () {
  afterEach(() => {
    getFileContent.mockReset();
  });

  it("returns true when the file content includes the text", async () => {
    getFileContent.mockResolvedValue("Hello, World!");

    const result = await findText("path", "Hello");

    expect(result).toBe(true);
  });

  it("returns false when the file content does not include the text", async () => {
    getFileContent.mockResolvedValue("Hello, World!");

    const result = await findText("path", "String");

    expect(result).toBe(false);
  });

  it("rejects the promise when getFileContent throws an error", async () => {
    getFileContent.mockRejectedValue(new Error("File not found"));

    await expect(findText("path", "hello")).rejects.toThrow("File not found");
  });
});
