const startServer = require("./simpleTestApp");
const supertest = require("supertest");

describe("simpleTestApp", function () {
  let server;

  beforeAll(async () => {
    server = await startServer();
  });

  afterAll(() => {
    console.log("Stopping the server...");
    server.stop();
    console.log("Server stopped");
  });

  it("should return an empty array at the app start", async () => {
    const res = await supertest(server).get("/");

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual([]);
  });

  it("should add a task to the array", async () => {
    const task = {
      title: "test",
      description: "desc",
    };

    const res = await supertest(server).post("/").send(task);

    expect(res.statusCode).toEqual(200);

    // const getRes = await supertest(server).get("/");
    // expect(getRes.body).toContainEqual(task);
  });

  it("should return 400 if the task is invalid", async () => {
    const task = {
      title: "t",
      description: "",
    };

    const res = await supertest(server).post("/").send(task);

    expect(res.statusCode).toEqual(400);
  });
});
