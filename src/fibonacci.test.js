const getFibonacci = require("./fibonacci");

describe("fibonacci", function () {
  const normalCases = [
    [6, 8],
    [10, 55],
  ];

  const edgeCases = [
    [-5, 0],
    [0, 0],
    [1, 1],
  ];

  describe("should handle normal cases correctly", function () {
    test.each(normalCases)("returns %i for input %i", (input, expected) => {
      expect(getFibonacci(input)).toEqual(expected);
    });
  });

  describe("should handle edge cases correctly", function () {
    test.each(edgeCases)("returns %i for input %i", (input, expected) => {
      expect(getFibonacci(input)).toEqual(expected);
    });
  });
});
